﻿using System.Collections.Generic;
using System.Linq;
using ProjektNotatki.Data.RepositoryService.Interface;

namespace ProjektNotatki.Data.RepositoryService.Implementation
{
    public class TagService : ITagService
    {
        private NotatkiEntities ctx;

        public TagService(NotatkiEntities notatki)
        {
            ctx = notatki;
        }
        public Tag Get(int id)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                return ctx1.Tag.First(x => x.Id == id);
            }
        }

        public IList<Tag> GetAll()
        {
            using (var ctx1= new NotatkiEntities())
            {
                return ctx1.Tag.ToList();   
            }

        }

        public void Create(Tag tag)
        {
                ctx.Tag.Add(tag);
                ctx.SaveChanges();
        }

        public void Update(Tag tag)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                ctx1.Configuration.LazyLoadingEnabled = false;
                var tagEntity = ctx1.Tag.FirstOrDefault(x => x.Id == tag.Id);
                if (tagEntity != null)
                {
                    tagEntity.Name = tag.Name;
                    //tagEntity.NoteXTag = tag.NoteXTag;
                }
                ctx1.Entry(tagEntity).State = System.Data.Entity.EntityState.Modified;
                ctx1.SaveChanges();
            }
        }

        public void Delete(Tag tag)
        {
                var tagEntity = ctx.Tag.First(x => x.Id == tag.Id);
                ctx.Tag.Remove(tagEntity);
                ctx.SaveChanges();
        }
    }
}
