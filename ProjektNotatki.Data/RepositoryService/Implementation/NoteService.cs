﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using ProjektNotatki.Data.RepositoryService.Interface;

namespace ProjektNotatki.Data.RepositoryService.Implementation
{
    internal class TagEqualComparer : IEqualityComparer<Tag>
    {
        public bool Equals(Tag x, Tag y)
        {
            if (x.Id == y.Id)
                return true;
            return false;
        }

        public int GetHashCode(Tag obj)
        {
            return obj.GetHashCode();
        }
    }

    public class NoteService : INoteService
    {
        private readonly NotatkiEntities ctx;

        public NoteService(NotatkiEntities notatki
             )
        {
            ctx = notatki;
        }
        public IList<Note> GetAll()
        {
            using (var ctx1 = new NotatkiEntities())
            {


                var note = ctx1.Note
                    .Include(x => x.Tag)
                    .Include(x => x.noteType)
                    .ToList();

                return note;
            }
        }

        public void Update(Note note)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                var querry = "DELETE FROM NoteXTag WHERE NoteId=" + note.Id;
                ctx1.Database.ExecuteSqlCommand(querry);

                var noteEntity = ctx1.Note.First(x => x.Id == note.Id);
                noteEntity.Content = note.Content;
                noteEntity.Title = note.Title;
                noteEntity.IsPasswordProtected = note.IsPasswordProtected;
                noteEntity.IsRemind = note.IsRemind;
                noteEntity.Remind = note.Remind;
                noteEntity.Password = note.Password;
                var tags  = new List<Tag>();

                foreach (var tag in note.Tag)
                {
                    tags.Add(ctx1.Tag.Find(tag.Id));
                }
                //noteEntity.Tag = null;
                noteEntity.Tag = tags;
                noteEntity.noteType = ctx1.noteType.First(x=>x.Id==note.noteType.Id);
               //ctx1.Entry(noteEntity).State = EntityState.Modified;
                
                ctx1.Note.AddOrUpdate(noteEntity);
                ctx1.SaveChanges();
            }
        }

       
        public void Delete(Note note)
        {
            var noteEntity = ctx.Note.First(x => x.Id == note.Id);
            ctx.Note.Remove(noteEntity);
            ctx.SaveChanges();
        }

        public void Create(Note note)
        {
            ctx.Note.Add(note);
            ctx.SaveChanges();
        }

        public Note Get(int id)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                return ctx1.Note
                      .Include(x => x.Tag)
                       .Include(x => x.noteType)
                    .FirstOrDefault(x => x.Id == id);
            }
        }
    }
}
