﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjektNotatki.Data.RepositoryService.Interface;

namespace ProjektNotatki.Data.RepositoryService.Implementation
{
    public class FolderService : IFolderService
    {
        private readonly NotatkiEntities ctx;

        public FolderService(NotatkiEntities notatki)
        {
            ctx = notatki;
        }

        public noteType Get(int id)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                return ctx1.noteType.First(x => x.Id == id);
            }
        }

        public IList<noteType> GetAll()
        {
            using (var ctx1 = new NotatkiEntities())
            {
                return ctx1.noteType.ToList();
            }
        }

        public void Create(noteType noteType)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                ctx1.noteType.Add(noteType);
                ctx1.SaveChanges();
                //ctx.Dispose();
            }
        }

        public void Update(noteType noteType)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                var noteTypeEntity = ctx1.noteType.FirstOrDefault(x => x.Id == noteType.Id);
                if (noteTypeEntity != null)
                {
                    noteTypeEntity.Name = noteType.Name;

                }
                ctx1.Entry(noteTypeEntity).State = System.Data.Entity.EntityState.Modified;
                ctx1.SaveChanges();
            }
        }

        public void Delete(noteType noteType)
        {
            using (var ctx1 = new NotatkiEntities())
            {
                var noteTypeEntity = ctx1.noteType.First(x => x.Id == noteType.Id);
                if (noteTypeEntity.Note.Count > 0)
                {
                    throw new Exception();
                }
                ctx1.noteType.Remove(noteTypeEntity);
                ctx1.SaveChanges();
            }
        }
    }
}
