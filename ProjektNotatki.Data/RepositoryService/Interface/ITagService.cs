﻿using System.Collections.Generic;

namespace ProjektNotatki.Data.RepositoryService.Interface
{
    public interface ITagService
    {
        Tag Get(int id);
        IList<Tag> GetAll();
        void Create(Tag tag);
        void Update(Tag tag);
        void Delete(Tag tag);
    }
}
