﻿using System.Collections.Generic;

namespace ProjektNotatki.Data.RepositoryService.Interface
{
    public interface INoteService
    {
        IList<Note> GetAll();
        void Update(Note note);
        void Delete(Note note);
        void Create(Note note);
        Note Get(int id);
    }
}
