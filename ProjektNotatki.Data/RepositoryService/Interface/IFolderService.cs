﻿using System.Collections.Generic;

namespace ProjektNotatki.Data.RepositoryService.Interface
{
    public interface IFolderService
    {
        noteType Get(int id);
        IList<noteType> GetAll();
        void Create(noteType noteType);
        void Update(noteType noteType);
        void Delete(noteType noteType);
    }
}
