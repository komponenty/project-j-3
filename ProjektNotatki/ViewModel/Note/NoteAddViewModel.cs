﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.DataModel;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class NoteAddViewModel : Screen, INoteAdd
    {
        private readonly ITagService _tagService;
        private readonly IFolderService _folderService;
        private readonly INoteService _noteService;
        private readonly IEventAggregator _eventAggregator;
        private Caliburn.PresentationFramework.IObservableCollection<TagDataModel> _tags;
        private Caliburn.PresentationFramework.IObservableCollection<FolderDataModel> _folders;
        private DateTime _reminder = DateTime.Now;
        private bool _IsPassword;
        private bool _isDateCheck;
        private string _password;
        private string _title;
        private string _content;

        public bool IsPassword
        {
            get
            {
                return _IsPassword;
            }
            set
            {
                _IsPassword = value;
                NotifyOfPropertyChange(() => IsPassword);
            }

        }

        public bool IsDate
        {
            get { return _isDateCheck; }
            set
            {
                _isDateCheck = value;
                NotifyOfPropertyChange(() => IsDate);
            }
        }

        public string Password { get { return _password; } set { _password = value; NotifyOfPropertyChange(() => Password); } }
        public string Title { get { return _title; } set { _title = value; NotifyOfPropertyChange(() => Title); } }
        public string Content { get { return _content; }set { _content = value; NotifyOfPropertyChange(() => Content); } }

        public NoteAddViewModel(ITagService tagService, IFolderService folderService,INoteService noteService,IEventAggregator eventAggregator)
        {
            _tagService = tagService;
            _folderService = folderService;
            _noteService = noteService;
            _eventAggregator = eventAggregator;

            Tags = new Caliburn.PresentationFramework.BindableCollection<TagDataModel>(_tagService.GetAll().Select(x => new TagDataModel(x)));
            Folders = new Caliburn.PresentationFramework.BindableCollection<FolderDataModel>(_folderService.GetAll().Select(x => new FolderDataModel(x)));

        }


        public DateTime Reminder
        {
            get
            {
                return _reminder;
            }
            set
            {
                _reminder = value;
                NotifyOfPropertyChange(() => Reminder);
            }
        }

        public Caliburn.PresentationFramework.IObservableCollection<TagDataModel> Tags
        {
            get
            {
                return _tags;
            }
            set
            {
                _tags = value;
                NotifyOfPropertyChange(() => Tags);
            }
        }

        public Caliburn.PresentationFramework.IObservableCollection<FolderDataModel> Folders
        {
            get
            {
                return _folders;
            }
            set
            {
                _folders = value;
                NotifyOfPropertyChange(() => Folders);
            }
        }

        public void SelectOne(FolderDataModel folder)
        {
            foreach (var notChecked in _folders)
            {
                if (notChecked.Id != folder.Id)
                {
                    notChecked.IsChecked = false;
                }
            }
            _folders.Refresh();
        }

        public void Add()
        {
            if (!_tags.Any(x => x.IsChecked) && !_folders.Any(x => x.IsChecked) && _title!=null && _content!=null)
            {
                if (!_tags.Any(x => x.IsChecked) && !_folders.Any(x => x.IsChecked))
                {
                    MessageBox.Show((Window) this.GetView(), "Musisz wybrać przynajmniej jeden tag i jeden folder!");
                }
                else
                {
                    MessageBox.Show((Window)this.GetView(), "Musisz nadać tytuł i treść!");
                }
            }
            else
            {
                var note = new Note
                {
                    Content = Content,
                    IsPasswordProtected = IsPassword,
                    IsRemind = IsDate,
                    noteType = _folderService.Get(_folders.First(x => x.IsChecked).Id),
                    Title = Title,
                    Password = Password,
                    Remind = Reminder
                };
                note.Tag= new List<Tag>();
                foreach (var tagDataModel in _tags.Where(x=>x.IsChecked))
                {
                    note.Tag.Add(_tagService.Get(tagDataModel.Id));
                }
                _noteService.Create(note);
                _eventAggregator.Publish(new NoteRefreshEvent
                {
                    IsAdded = true
                });
                this.TryClose();
            }
            
        }


        public void RefreshItems()
        {
            Tags = new Caliburn.PresentationFramework.BindableCollection<TagDataModel>(_tagService.GetAll().Select(x => new TagDataModel(x)));
            Folders = new Caliburn.PresentationFramework.BindableCollection<FolderDataModel>(_folderService.GetAll().Select(x => new FolderDataModel(x)));
        }
    }
}
