﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.DataModel;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class NoteViewModel : Screen, INote, IHandle<NoteRefreshEvent>
    {
        private readonly INoteService _noteService;
        private readonly IWindowManager _windowManager;
        private readonly IGenericDataRepository<Note> _noteRepository;
        private readonly INoteAdd _noteAdd;
        private readonly INotePassword _notePassword;
        private readonly INoteMain _noteMain;
        private readonly INoteUpdate _noteUpdate;

        private Caliburn.PresentationFramework.IObservableCollection<NoteDataModel> _items;


        public NoteViewModel(
            INoteService noteService,
            IWindowManager windowManager,
            IGenericDataRepository<Note> noteRepository,
            INoteAdd noteAdd,
            IEventAggregator eventAggregator,
            INotePassword notePassword,
            INoteMain noteMain,
            INoteUpdate noteUpdate)
        {
            _noteService = noteService;
            _windowManager = windowManager;
            _noteRepository = noteRepository;
            _noteAdd = noteAdd;
            _notePassword = notePassword;
            _noteMain = noteMain;
            _noteUpdate = noteUpdate;
            eventAggregator.Subscribe(this);

            Items = new Caliburn.PresentationFramework.BindableCollection<NoteDataModel>(_noteService.GetAll().Select(x => new NoteDataModel(x)));
        }

        public void Remove(NoteDataModel child)
        {
            var note = _noteService.Get(child.Id);
            _noteService.Delete(note);

            Items = new Caliburn.PresentationFramework.BindableCollection<NoteDataModel>(_noteService.GetAll().Select(x => new NoteDataModel(x)));
        }

        public void Show(NoteDataModel child)
        {
            if (child.IsPasswordProtected)
            {
                _notePassword.LoadNote(_noteService.Get(child.Id));
                _windowManager.ShowWindow(_notePassword);
                return;

            }
            _noteMain.LoadNote(_noteService.Get(child.Id));
            _windowManager.ShowWindow(_noteMain);
        }

        public Caliburn.PresentationFramework.IObservableCollection<NoteDataModel> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }
        public void LoadNotes(IList<Note> notes)
        {
            Items =
                new Caliburn.PresentationFramework.BindableCollection<NoteDataModel>(
                    notes.Select(x => new NoteDataModel(x)));
        }

        public void Update(NoteDataModel child)
        {
            var note = _noteService.Get(child.Id);
            _noteUpdate.RefreshItems();
            _noteUpdate.LoadNote(note);
            _windowManager.ShowWindow(_noteUpdate);
        }

        public void Add()
        {
            _noteAdd.RefreshItems();
            _windowManager.ShowWindow(_noteAdd);
        }

        public void Handle(NoteRefreshEvent message)
        {
            if (message.IsAdded)
            {
                Items =
                    new Caliburn.PresentationFramework.BindableCollection<NoteDataModel>(
                        _noteService.GetAll().Select(x => new NoteDataModel(x)));
            }
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            //Items = new Caliburn.PresentationFramework.BindableCollection<NoteDataModel>(_noteService.GetAll().Select(x => new NoteDataModel(x)));

        }
    }
}
