﻿using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class NotePasswordViewModel : Screen, INotePassword
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IWindowManager _windowManager;
        private readonly INoteMain _noteMain;
        private readonly INoteService _noteService;
        private string _password = "";

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }

        public NotePasswordViewModel(
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            INoteMain noteMain,
            INoteService noteService)
        {
            _eventAggregator = eventAggregator;
            _windowManager = windowManager;
            _noteMain = noteMain;
            _noteService = noteService;
        }

        private Note _note;
        public void LoadNote(Note note)
        {
            _note = note;
        }

        public void Check()
        {
            if ((Password == _note.Password) || _note.Password == null)
            {
                _noteMain.LoadNote(_note);
                _windowManager.ShowWindow(_noteMain);
                TryClose();
            }
            else
            {
                MessageBox.Show("Hasło jest błędne!");
            }
        }

    }
}
