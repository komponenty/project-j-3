﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.ViewModel.Interface;
using MailSender.Service.Interface;

namespace ProjektNotatki.ViewModel
{
    public class NoteMainViewModel : Screen, INoteMain
    {
        private readonly IMailSender _mailSender;
        private readonly IWindowManager _windowManager;
        private Note _noteMain;
        public void LoadNote(Note note)
        {
            _noteMain = note;
            Content = note.Content;
            Title = note.Title;
            if (note.Remind == null)
            {
                Date = "Brak";
            }
            else
            {
                Date = string.Format("{0:u}", note.Remind);
            }
        }

        private string _content = "";
        private string _title = "";
        private string _emil = "";
        private string _date = "";

        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public string Date { get { return _date; } set { _date = value; NotifyOfPropertyChange(() => Date); } }

        public string Title { get { return _title; } set { _title = value; NotifyOfPropertyChange(() => Title); } }
        public string Email { get { return _emil; } set { _emil = value; NotifyOfPropertyChange(() => Email); } }

        public NoteMainViewModel(IMailSender mailSender, IWindowManager windowManager)
        {
            _mailSender = mailSender;
            _windowManager = windowManager;
        }

        public void Close()
        {
            TryClose();
        }

        public void Send()
        {
            try
            {
                var client = _mailSender.ConfigureSmtpClient("projektNotatki@Gmail.com", "ppp12345_");
                var message = _mailSender.CreateMailMessage("projektNotatki@Gmail.com", new List<string>() { { Email } },
                    Title,
                    Content);
                _mailSender.MailSend(client, message);
                TryClose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Coś poszło nie tak podczas wysyłki...");
            }
        }
    }
}
