﻿using System.Security.Cryptography.X509Certificates;
using ProjektNotatki.Configuration;
using ProjektNotatki.Data;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface ITagUpdate : IDependency
    {
        void LoadTag(Tag tag);
    }
}
