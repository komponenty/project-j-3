﻿using ProjektNotatki.Configuration;
using ProjektNotatki.Data;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface IFolderUpdate : ISingletonDependency
    {
        void LoadFolder(noteType noteType);
    }
}
