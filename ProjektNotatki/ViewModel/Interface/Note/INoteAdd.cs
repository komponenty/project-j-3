﻿using System.Security.Cryptography.X509Certificates;
using ProjektNotatki.Configuration;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface INoteAdd : IDependency
    {
       void RefreshItems();
    }
}
