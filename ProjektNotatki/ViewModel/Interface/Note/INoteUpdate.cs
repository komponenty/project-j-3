﻿using ProjektNotatki.Configuration;
using ProjektNotatki.Data;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface INoteUpdate : ISingletonDependency
    {
        void RefreshItems();
        void LoadNote(Note note);
    }
}
