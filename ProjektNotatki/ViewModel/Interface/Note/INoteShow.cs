﻿using ProjektNotatki.Configuration;
using ProjektNotatki.Data;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface INoteShow : ISingletonDependency
    {
        void LoadNote(Note note);
    }
}
