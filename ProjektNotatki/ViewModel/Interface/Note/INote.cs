﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using ProjektNotatki.Configuration;
using ProjektNotatki.Data;

namespace ProjektNotatki.ViewModel.Interface
{
    public interface INote : ISingletonDependency
    {
        void LoadNotes(IList<Note> notes);
    }
}
