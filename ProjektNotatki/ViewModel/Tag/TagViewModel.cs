﻿using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class TagViewModel : PropertyChangedBase, ITag, IHandle<TagRefreshEvent>
    {
        private readonly ITagService _tagService;
        private readonly ITagAdd _tagAdd;
        private readonly IEventAggregator _eventAggregator;
        private readonly IWindowManager _windowManager;
        private readonly ITagUpdate _tagUpdate;
        private readonly INote _note;
        private readonly INoteService _noteService;

        private Caliburn.PresentationFramework.BindableCollection<Tag> _items;

        public Caliburn.PresentationFramework.BindableCollection<Tag> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        public TagViewModel(
            ITagService tagService,
            ITagAdd tagAdd,
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            ITagUpdate tagUpdate,
            INote note,
            INoteService noteService)
        {
            _tagService = tagService;
            _tagAdd = tagAdd;
            _eventAggregator = eventAggregator;
            _windowManager = windowManager;
            _tagUpdate = tagUpdate;
            _note = note;
            _noteService = noteService;
            eventAggregator.Subscribe(this);

            Items = new Caliburn.PresentationFramework.BindableCollection<Tag>(_tagService.GetAll());
        }

        public void Remove(Tag child)
        {

            Items.Remove(child);
            _tagService.Delete(child);

        }

        public void Update(Tag child)
        {
            _tagUpdate.LoadTag(child);
            _windowManager.ShowWindow(_tagUpdate);
        }

        public void Add()
        {

            _windowManager.ShowWindow(_tagAdd);
        }

        public void Show(Tag child)
        {
            var notes = _noteService.GetAll();
            var noteList = new List<Note>();
            foreach (var note in notes)
            {
                foreach (var tag in note.Tag)
                {
                    if (tag.Id == child.Id)
                    {
                        noteList.Add(note);
                    }
                }
            }
            if (noteList.Count > 0)
            {
                _note.LoadNotes(noteList);
                _eventAggregator.Publish(new WindowChangeEvent()
                {
                    WindowToInitialize = _note
                });
            }
            else
            {
                MessageBox.Show("Brak notatek powiązanych z tym tagiem!");
            }
        }

        public void Handle(TagRefreshEvent message)
        {
            if (message.IsAdded)
            {
                Items = new Caliburn.PresentationFramework.BindableCollection<Tag>(_tagService.GetAll());
            }
        }
    }
}
