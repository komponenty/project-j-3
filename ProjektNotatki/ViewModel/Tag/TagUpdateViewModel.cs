﻿using System;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class TagUpdateViewModel :PropertyChangedBase, ITagUpdate, IViewAware
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ITagService _tagService;

        public TagUpdateViewModel(IEventAggregator eventAggregator, ITagService tagService)
        {
            _eventAggregator = eventAggregator;
            _tagService = tagService;
        }

        private Tag TagToUpdate { get; set; }

        public void LoadTag(Tag tag)
        {
            TagToUpdate = tag;
            _name = TagToUpdate.Name;
        }

        public void Update()
        {
            TagToUpdate.Name = _name;
            _tagService.Update(TagToUpdate);
            _eventAggregator.Publish(new TagRefreshEvent
            {
                IsAdded = true
            });
            _dialogWindow.Close();
        }

        private string _name = "";

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private Window _dialogWindow;

        public void AttachView(object view, object context = null)
        {
            _dialogWindow = view as Window;
            if (ViewAttached != null)
                ViewAttached(this,
                   new ViewAttachedEventArgs() { Context = context, View = view });
        }

        public object GetView(object context = null)
        {
            return _dialogWindow;
        }

        public event EventHandler<ViewAttachedEventArgs> ViewAttached;
    }
}
