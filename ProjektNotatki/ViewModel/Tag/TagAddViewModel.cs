﻿using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class TagAddViewModel : PropertyChangedBase, ITagAdd
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ITagService _tagService;
        private readonly IWindowManager _windowManager;

        public TagAddViewModel(
            IEventAggregator eventAggregator,
            ITagService tagService,
            IWindowManager windowManager)
        {
            _eventAggregator = eventAggregator;
            _tagService = tagService;
            _windowManager = windowManager;
            _eventAggregator.Subscribe(this);
        }


        public TagRefreshEvent tagEvent { get; set; }

        public void Add()
        {
            _tagService.Create(new Tag
            {
                Name = _name
            });
            _eventAggregator.Publish(new TagRefreshEvent
            {
                IsAdded = true
            });

        }

        private string _name = "";

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(()=>Name);
            }
        }
    }
}
