﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using Caliburn.Micro;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class ShellViewModel : Conductor<object>, IShell, IHandle<WindowChangeEvent>
    {
        private readonly INote _notesView;
        private readonly ITag _tagView;
        private readonly IFolder _folderView;
        private readonly IEventAggregator _eventAggregator;
        private readonly INoteService _noteService;
        private readonly ISearch _search;

        BackgroundWorker _worker = new BackgroundWorker();

        public ShellViewModel(INote notesView, ITag tagView, IFolder folderView, IEventAggregator eventAggregator, INoteService noteService, ISearch search)
        {
            _notesView = notesView;
            _tagView = tagView;
            _folderView = folderView;
            _eventAggregator = eventAggregator;
            _noteService = noteService;
            _search = search;
            DisplayName = "Notatki";

            _eventAggregator.Subscribe(this);

            this.ActivateItem(_notesView);
            _worker.DoWork += Reminder;
            _worker.RunWorkerAsync(10000000);
        }

        public void OpenNote()
        {
            // Windows++;
            // AditionalViewModel a = new AditionalViewModel();
            _notesView.LoadNotes(_noteService.GetAll());
            ActivateItem(_notesView);
            //WindowManager wm = new WindowManager();
            //wm.ShowWindow(a);
            //a.Show();

        }

        public void OpenTag()
        {
            ActivateItem(_tagView);
        }

        public void OpenFolder()
        {
            ActivateItem(_folderView);
        }

        public void OpenSearch()
        {
            ActivateItem(_search);
        }

        public void Handle(WindowChangeEvent message)
        {
            ActivateItem(message.WindowToInitialize);
        }

        void Reminder(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                var notes = _noteService.GetAll();
                foreach (var note in notes)
                {
                    if (note.IsRemind && note.Remind != null)
                    {
                        var rem = (DateTime)note.Remind;
                        if (rem.DayOfYear == DateTime.Now.DayOfYear)
                        {
                            MessageBox.Show("Masz na dzisiaj zaplanowane notatki!");
                            System.Threading.Thread.Sleep(1000000000);
                        }
                    }
                }
            }
        }
    }
}
