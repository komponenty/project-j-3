﻿using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class SearchViewModel : Screen, ISearch
    {
        private readonly INoteService _noteService;
        private readonly IEventAggregator _eventAggregator;
        private readonly INote _note;

        public SearchViewModel(
            INoteService noteService,
            IEventAggregator eventAggregator,
            INote note)
        {
            _noteService = noteService;
            _eventAggregator = eventAggregator;
            _note = note;
        }

        private string _title = "";
        private string _content = "";
        public string Title { get { return _title; } set { _title = value; NotifyOfPropertyChange(() => Title); } }
        public string Content { get { return _content; } set { _content = value; NotifyOfPropertyChange(() => Content); } }

        public void Search()
        {
            Content=Content.Replace("\r", string.Empty).Replace("\n", string.Empty);
            var notes = _noteService.GetAll().Where(x => (x.Content.Contains(Content)) && (x.Title.Contains(Title))).ToList();
            if (!notes.Any())
            {
                MessageBox.Show("Brak wyników wyszukiwania!");
            }
            else
            {
                _note.LoadNotes(notes);
                _eventAggregator.Publish(new WindowChangeEvent
                {
                    WindowToInitialize = _note
                });
            }
        }
    }
}
