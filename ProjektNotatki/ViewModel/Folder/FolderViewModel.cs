﻿using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class FolderViewModel : PropertyChangedBase, IFolder, IHandle<FolderRefreshEvent>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IFolderService _folderService;
        private readonly IFolderAdd _folderAdd;
        private readonly IFolderUpdate _folderUpdate;
        private readonly IWindowManager _windowManager;
        private readonly INoteService _noteService;
        private readonly INote _note;


        private Caliburn.PresentationFramework.IObservableCollection<noteType> _items;

        public Caliburn.PresentationFramework.IObservableCollection<noteType> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        public FolderViewModel(
            IEventAggregator eventAggregator,
            IFolderService folderService,
            IFolderAdd folderAdd,
            IFolderUpdate folderUpdate,
            IWindowManager windowManager,
            INoteService noteService,
            INote note)
        {
            _eventAggregator = eventAggregator;
            _folderService = folderService;
            _folderAdd = folderAdd;
            _folderUpdate = folderUpdate;
            _windowManager = windowManager;
            _noteService = noteService;
            _note = note;

            _eventAggregator.Subscribe(this);
            Items = new Caliburn.PresentationFramework.BindableCollection<noteType>(_folderService.GetAll());
        }

        public void Add()
        {
            _windowManager.ShowWindow(_folderAdd);
        }

        public void Remove(noteType child)
        {
            try
            {
                _folderService.Delete(child);
                Items.Remove(child);
            }
            catch
            {
                MessageBox.Show("Nie można usunąć tego folderu!");
            }
        }

        public void Update(noteType child)
        {
            _folderUpdate.LoadFolder(child);
            _windowManager.ShowWindow(_folderUpdate);
        }

        public void Show(noteType child)
        {
            var notes = _noteService.GetAll();
            var noteList = notes.Where(note => note.noteType.Id == child.Id).ToList();
            if (noteList.Count > 0)
            {
                _note.LoadNotes(noteList);
                _eventAggregator.Publish(new WindowChangeEvent()
                {
                    WindowToInitialize = _note
                });
            }
            else
            {
                MessageBox.Show("Brak notatek powiązanych z tym folderem!");
            }
        }

        public void Handle(FolderRefreshEvent message)
        {
            if (message.IsAdded)
            {
                Items = new Caliburn.PresentationFramework.BindableCollection<noteType>(_folderService.GetAll());
            }
        }
    }
}
