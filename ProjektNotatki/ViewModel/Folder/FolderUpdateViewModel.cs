﻿using System;
using System.Windows;
using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class FolderUpdateViewModel :PropertyChangedBase, IFolderUpdate, IViewAware
    {
        private readonly IFolderService _folderService;
        private readonly IEventAggregator _eventAggregator;
        private noteType NoteToUpdate { get; set; }

        public FolderUpdateViewModel(IFolderService folderService,IEventAggregator eventAggregator)
        {
            _folderService = folderService;
            _eventAggregator = eventAggregator;
        }

        public void LoadFolder(noteType noteType)
        {
            NoteToUpdate = noteType;
            _name = noteType.Name;
        }

        public void Update()
        {
          NoteToUpdate.Name = _name;
            _folderService.Update(NoteToUpdate);
            _eventAggregator.Publish(new FolderRefreshEvent
            {
                IsAdded = true
            });
            _dialogWindow.Close();
        }

        private string _name = "";

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private Window _dialogWindow;

        public void AttachView(object view, object context = null)
        {
            _dialogWindow = view as Window;
            if (ViewAttached != null)
                ViewAttached(this,
                   new ViewAttachedEventArgs() { Context = context, View = view });
        }

        public object GetView(object context = null)
        {
            return _dialogWindow;
        }

        public event EventHandler<ViewAttachedEventArgs> ViewAttached;
    }
}
