﻿using Caliburn.Micro;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.Events;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.ViewModel
{
    public class FolderAddViewModel :PropertyChangedBase, IFolderAdd
    {
                private readonly IEventAggregator _eventAggregator;
        private readonly IWindowManager _windowManager;
        private readonly IFolderService _folderService;

        public FolderAddViewModel(
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            IFolderService folderService)
        {
            _eventAggregator = eventAggregator;
            _windowManager = windowManager;
            _folderService = folderService;
            _eventAggregator.Subscribe(this);
           
        }


        public FolderRefreshEvent tagEvent { get; set; }

        public void Add()
        {
            _folderService.Create(new noteType
            {
                Name = _name
            });
            _eventAggregator.Publish(new FolderRefreshEvent
            {
                IsAdded = true
            });
        }

        private string _name = "";

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(()=>Name);
            }
        }
    }
}
