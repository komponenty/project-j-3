﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Caliburn.Micro;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MailSender.Service.Interface;
using ProjektNotatki.Data;
using ProjektNotatki.Data.RepositoryService.Implementation;
using ProjektNotatki.Data.RepositoryService.Interface;
using ProjektNotatki.ViewModel.Interface;

namespace ProjektNotatki.Configuration
{
    public class AppBootstrapper : Bootstrapper<IShell>
    {
        private IWindsorContainer _windsorContainer;

        protected override void Configure()
        {
            _windsorContainer = new WindsorContainer();
            //stuff od caliburna
            _windsorContainer.Register(Component.For<IWindowManager>().ImplementedBy<WindowManager>());
            _windsorContainer.Register(Component.For<IEventAggregator>().ImplementedBy<EventAggregator>());


            //serwisy bazodanowe
            _windsorContainer.Register(Component.For<ITagService>()
                .ImplementedBy<TagService>()
                .LifeStyle.Is(LifestyleType.Transient));

            _windsorContainer.Register(Component.For<IFolderService>()
                .ImplementedBy<FolderService>()
                .LifeStyle.Is(LifestyleType.Transient));

            _windsorContainer.Register(Component.For<INoteService>()
                .ImplementedBy<NoteService>()
                .LifeStyle.Is(LifestyleType.Transient));

            _windsorContainer.Register(
                Component.For(typeof(IGenericDataRepository<>))
                    .ImplementedBy(typeof(GenericDataRepository<>))
                    .LifeStyle.Transient);

            //rejestrowanie przez konwencje...
            //w sumie singleton czy transistent nie ma tutaj znaczenia, okna i 
            //tak nie podlegaja niszczeniu wiec i tak spelniamy zaleznosci tylko raz
            _windsorContainer.Register(
                AllTypes.FromThisAssembly()
                    .BasedOn<ISingletonDependency>()
                    .WithService.DefaultInterface()
                    .Configure(c => c.LifeStyle.Singleton));


            _windsorContainer.Register(
                AllTypes.FromThisAssembly()
                    .BasedOn<IDependency>()
                    .WithService.DefaultInterface()
                    .Configure(c => c.LifeStyle.Transient));

            _windsorContainer.Register(Component.For<IMailSender>()
                .ImplementedBy<MailSender.Service.Implementation.MailSender>()
                .LifeStyle.Is(LifestyleType.Singleton));

            //data context, using jest gdzieniegdzie potrzebny niestety
            //przez łańcych wywołań 
            _windsorContainer.Register(
                Component.For(typeof (NotatkiEntities))
                    .ImplementedBy(typeof (NotatkiEntities))
                    .LifeStyle.Is(LifestyleType.Singleton));
            var eventAggregator = _windsorContainer.Resolve<IEventAggregator>();

            //var messageHandlers = _windsorContainer.ResolveAll<IMessageHandler>();
            //foreach (var messageHandler in messageHandlers)
            //{
            //    eventAggregator.Subscribe(messageHandler);
            //}
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies();
        }

        protected override object GetInstance(Type service, string key)
        {
            return string.IsNullOrWhiteSpace(key) ? _windsorContainer.Resolve(service) : _windsorContainer.Resolve(key, new { });
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _windsorContainer.ResolveAll(service).Cast<object>();
        }

        protected override void BuildUp(object instance)
        {
            instance.GetType().GetProperties()
                .Where(property => property.CanWrite && property.PropertyType.IsPublic)
                .Where(property => _windsorContainer.Kernel.HasComponent(property.PropertyType))
                .ForEach(property => property.SetValue(instance, _windsorContainer.Resolve(property.PropertyType), null));
        }
    }
}
