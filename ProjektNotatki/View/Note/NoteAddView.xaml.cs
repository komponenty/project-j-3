﻿using System.Windows;

// ReSharper disable once CheckNamespace
namespace ProjektNotatki.ViewModel
{
    /// <summary>
    /// Interaction logic for NoteAddView.xaml
    /// </summary>
    public partial class NoteAddView : Window
    {
        public NoteAddView()
        {
            InitializeComponent();
        }
    }
}
