﻿using ProjektNotatki.Data;

namespace ProjektNotatki.DataModel
{
    public class FolderDataModel
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Name { get; set; }

        public FolderDataModel(noteType noteType)
        {
            Name = noteType.Name;
            Id = noteType.Id;
            IsChecked = false;
        }
    }
}
