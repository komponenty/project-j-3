﻿using ProjektNotatki.Data;

namespace ProjektNotatki.DataModel
{
    //klasa pomagajaca w prezentacji danych dla notatek
    public class NoteDataModel
    {
        public int Id { get; set; }
        public string Tags { get; set; }
        public string Folder { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsPasswordProtected { get; set; }
        public string Password { get; set; }

        public NoteDataModel (Note note)
        {
            Id = note.Id;
            foreach (var tagx in note.Tag)
            {
                Tags += tagx.Name + ",";
            }
            Folder = note.noteType.Name;
            Title = note.Title;
            Content = note.Content;
            IsPasswordProtected = note.IsPasswordProtected;
            Password = note.Password;
        }
    }

}
