﻿using ProjektNotatki.Data;

namespace ProjektNotatki.DataModel
{
    public class TagDataModel
    {
        public bool IsChecked { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }

        public TagDataModel(Tag tag)
        {
            Id = tag.Id;
            Name = tag.Name;
            IsChecked = false;
        }
    }
}
