﻿namespace ProjektNotatki.Events
{
    public class WindowChangeEvent
    {
        public object WindowToInitialize { get; set; }
    }
}
