﻿namespace ProjektNotatki.Events
{
    public class NoteRefreshEvent
    {
        public bool IsAdded { get; set; }
    }

}
