﻿namespace ProjektNotatki.Events
{
    public class FolderRefreshEvent
    {
        public bool IsAdded { get; set; }
    }
}
