﻿namespace ProjektNotatki.Events
{
    //klasy do przesylania wiadomosci miedzy komponentami, nie moga byc zarzadzane przez kontener
    public class TagRefreshEvent
    {
        public bool IsAdded { get; set; }
    }
}
